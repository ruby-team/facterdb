========
facterdb
========

----------------------------------------------------
Get a JSON fact set from the facterdb facts database
----------------------------------------------------

:Author: Gabriel Filion
:Date: 2020
:Manual section: 1

Synopsis
========

| facterdb <json_filter>

Description
===========

The ``facterdb`` tool lets you obtain a JSON-formatted data structure from the
facts database that the facterdb library provides. Facts are selected using a
filter properly formatted for jgrep.

Environment variables
=====================

| **FACTERDB_SEARCH_PATHS**
|     List of paths that contain external facts that will complement the
|     default facterdb database.
|
|     When separating paths please use the default path separator character
|     supported by your OS:
|
|     - Unix/Linux/OSX = :
|     - Windows = ;
|
|     Each fact set you create must meet the following requirements:
|
|     - A JSON serialized file containing a single Hash of all the facts.
|     - The facts file must end in `.facts`
|     - Must be placed inside some directory. You can organize this directory
|       however you like.

| **FACTERDB_SKIP_DEFAULTDB**
|     If this is variable is set (to any value), it will instruct facterdb to
|     not look at its built-in facts which can be useful should you need to
|     completely replace which facts are used. You would most likely use this
|     in combination with the `FACTERDB_SEARCH_PATHS` environment variable.

| **FACTERDB_INJECT_SOURCE**
|     If this environment variable is set to any value, the following facts
|     are injected into all fact sets:
|
|    - `_facterdb_path`: The base name of the file used to load this fact set
|      e.g. `centos-5-i386.facts`
|    - `_facterdb_filename` : The full path of the file used to load this fact
|      set e.g. `/project/facter-db/centos-5-i386.facts`

Examples
========

Get facts for facter 2.4 that are either from Debian 7 or RedHat 7::

        facterdb 'facterversion=/^2.4\./ and (operatingsystem=Debian and operatingsystemrelease>=7 or operatingsystem=RedHat and operatingsystemrelease=/^7/)'

See also
========

``jgrep(1)``
